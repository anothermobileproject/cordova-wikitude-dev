# cordova-wikitude-dev
Wikitudes developer plugin for iOS 10

Wikitude is the process of updating their plugin for iOS. This repo contains their July 26 dev release, and was created to easily install the plugin via cordova command line.

Please note, there is no gaurantee that this repo will be maintained. This repo will NOT be monitored for issue reporting or pull requests.


#### Always check for updates on Wikitude's [download](http://www.wikitude.com/download/) page. 
